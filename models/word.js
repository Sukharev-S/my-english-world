let mongoose = require("mongoose");

let wordSchema = new mongoose.Schema({
  name: String,
  translation: String,
  image: String,
  part_of_speech: String,
  description: String,
  example: String,
  transcript: String,
  level: String,
  created: {type: Date, default: Date.now}
});

module.exports = mongoose.model("Word", wordSchema); 